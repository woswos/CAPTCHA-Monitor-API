#!/usr/bin/env bash

npm_root=`npm root -g`
apidoc_root="${npm_root}/apidoc/template-single"

# ref: https://askubuntu.com/a/30157/8698
if ! [ $(id -u) = 0 ]; then
   echo -e "--> The script need to be run as root <-- \n" >&2
   exit 1
fi

echo "> Creating the backup of the original template"
mv -v "${apidoc_root}/index.html" "${apidoc_root}/index_backup.html"
echo " "

echo "> Overwriting the template"
cp -v "apidoc_template.html" $apidoc_root
mv -v "${apidoc_root}/apidoc_template.html" "${apidoc_root}/index.html"
echo " "

echo "> Generating the docs"
eval 'apidoc -e "(node_modules|docs)" -o docs -v --single'
echo " "

echo "> Restoring the original template"
mv -v "${apidoc_root}/index_backup.html" "${apidoc_root}/index.html"
echo " "

echo "> Done"

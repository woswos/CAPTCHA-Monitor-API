# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.2] - 2020-07-28
### Added
- `count_only` metaparameter for easily getting number of returned results without
wasting bandwidth


## [0.0.1] - 2020-07-22
### Added
- The very first working version of the API
- Documentation
- This CHANGELOG file
- README file explanations


[0.0.2]: https://gitlab.torproject.org/woswos/CAPTCHA-Monitor-API/-/merge_requests/1/diffs
[0.0.1]: https://gitlab.torproject.org/woswos/CAPTCHA-Monitor-API/-/commit/9272917f3f80999c44207bee8d032f10e7443fd2
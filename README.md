# CAPTCHA Monitor's API

The API is mainly used to feed the graphs/tables on the dashboard, but it also 
servers as a direct data source for individuals who are interested in conduct 
further research using the dataset.

The live version of the API and its documentation is avaliable at 
[api.captcha.wtf](https://api.captcha.wtf/).

See https://gitlab.torproject.org/woswos/CAPTCHA-Monitor/-/wikis/home for the
main project and further details.

## Installation
1. Clone this repository
1. cd into the cloned repository
1. Run `npm install` inside of the repository root
1. Start the server with `npm run start` and you are ready to go at `://localhost:3002`!

## Development 
1. Run `npm install --dev` instead of `npm install` to install development dependencies
1. Start the server with `npm run dev` and you are ready to go at `://localhost:3002`!

## Generating the documentation
1. Install apiDoc via `npm install apidoc -g`
1. cd into the project folder
1. Run `npm run docs`
    - You need to run this command with `sudo` privileges because we need to monkey patch
    the apiDoc template to get rid of the webfonts and other bunch of tracking code. We do
    this by temporarily replacing the template file within the package with `apidoc_template.html`
    file.
1. The documentation will be ready in /docs folder

## Contributing
Feel free to create an issue or have a merge request if you have any suggestions.
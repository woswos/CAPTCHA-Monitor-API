const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet')
const compression = require('compression')
const rateLimit = require('express-rate-limit')
const timeout = require('connect-timeout')

const app = express();

// Timeout queries that take more than 120 seconds
app.use(timeout('120s'))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// 500 requests per minute
const limiter = rateLimit({
    windowMs: 1 * 60 * 1000,
    max: 500,
})

app.use(cors())
app.use(compression());
app.use(helmet());
app.use(limiter)

// Define routes
app.use('/relays', require('./app/routes/relays'));
app.use('/measurements', require('./app/routes/measurements'));
app.use('/tests', require('./app/routes/tests'));
app.use('/dashboard', require('./app/routes/dashboard'));

// Serve the documentation at the root
app.use(express.static('docs'));

// Home page
// app.get('/', (req, res, next) => {
//     res.status(200).json({
//         message: 'CAPTCHA Monitor API'
//     });
// });


/**
@api {get} /version Get the current version of the API
@apiVersion 0.0.1
@apiName GetAPIVersion
@apiGroup API

@apiSuccess {String} version The current version of the API

@apiError (500 Internal Server Error) InternalServerError The server encountered an internal error
*/
app.get('/version', (req, res, next) => {
    res.status(200).json({
        version: '0.0.2'
    });
});


// 404 handler
app.use((req, res, next) => {
    res.status(404).send({
        error: {
            status: 404,
            message: 'Not Found'
        }
    })
});


// Error handler
app.use((error, req, res, next) => {
    res.status(error.status || 500).send({
        error: {
            status: error.status || 500,
            message: error.message || 'Internal Server Error',
        },
    });
});

// Timeout function
function haltOnTimedout (req, res, next) {
  if (!req.timedout) next()
}

// Start server
const PORT = process.env.PORT || 3002;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
})

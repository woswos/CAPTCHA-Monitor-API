require('dotenv').config()

const {
    Pool
} = require('pg')

const connectionString = `postgresql://${process.env.CM_DB_USER}:${process.env.CM_DB_PASS}@${process.env.CM_DB_HOST}:${process.env.CM_DB_PORT}/${process.env.CM_DB_NAME}`

const pool = new Pool({
    connectionString: connectionString
})

const rename_query_params = (query, params) => {
    let param_count_in_query = (query.match(/%s/g) || []).length;

    if(param_count_in_query == params.length){
        for(let i = 1; i <= param_count_in_query; i++){
            query = query.replace('%s', '$' + i);
        }
    }else{
        throw 'Number of parameters in the query doesn\'t match the number of provided parameters';
    }

    return query;
}

const append_where = (query, param) => {
    if(query.includes('WHERE')){
        query += ' AND '
    } else{
        query += ' WHERE '
    }

    return query += param;
}

const classify_by_date = (rows, attribute, max_days_prior = 30) => {

    let today = new Date();
    let labels = [];
    let data_bins = {};

    // Figure out the classes by finding the unique values
    for (let i = 0; i < rows.length; i++) {
        data_bins[rows[i][attribute]] = []
    }

    // Iterate for classifying data for each wanted day
    for (let m = max_days_prior; m > 0; m--) {
        let selected_date = new Date();
        selected_date.setDate(selected_date.getDate() - m);
        //console.log('looking for m days back', m)

        // Iterate for classifying data for each data bin
        for (let data_bins_key in data_bins) {

            let total_count = 0;
            let captcha_faced_count = 0;
            let captcha_faced_percentage = 0;
            let date;

            // Find the ones that match the day and data bin
            for (let i = 0; i < rows.length; i++) {
                date = new Date(Date.parse(rows[i].timestamp));

                // Compare the days between today and the given date
                if (daysBetween(date, today) == m) {
                    selected_date = date

                    // Find the ones that match the data bin
                    if (rows[i][attribute] == data_bins_key) {

                        //console.log('selected_date: ', selected_date, '->', rows[i][attribute], '-> count: ', rows[i].count, '-> CAPTCHA?: ', rows[i].is_captcha_found)

                        total_count += Number(rows[i].count);

                        if (rows[i].is_captcha_found == '1') {
                            captcha_faced_count += Number(rows[i].count);
                        }

                    }

                }
            }

            captcha_faced_percentage = ((captcha_faced_count / total_count) * 100).toFixed(2)

            // Populate the data bins
            data_bins[data_bins_key].push(captcha_faced_percentage);

        }

        labels.push(dateFormat(selected_date));
    }

    return {
        data_bins: data_bins,
        labels: labels
    };
}

const classify_for_pie_chart = (rows, attribute) => {

    let labels = [];
    let res = [];
    let data_bins = {};

    // Figure out the classes by finding the unique values
    for (let i = 0; i < rows.length; i++) {
        data_bins[rows[i][attribute]] = []
    }

    // Iterate for classifying data for each data bin
    let key_id = 0;

    for (let data_bins_key in data_bins) {

        let total_count = 0;
        let captcha_faced_count = 0;
        let captcha_faced_percentage = 0;

        for (let i = 0; i < rows.length; i++) {
            // Find the ones that match the data bin
            if (rows[i][attribute] == data_bins_key) {

                total_count += Number(rows[i].count);

                if (rows[i].is_captcha_found == '1') {
                    captcha_faced_count += Number(rows[i].count);
                }

            }
        }

        captcha_faced_percentage = ((captcha_faced_count / total_count) * 100).toFixed(2)

        // Populate the data bins
        //data_bins[data_bins_key].push(captcha_faced_percentage);
        //data_bins[data_bins_key][key_id] = captcha_faced_count;

        res.push(captcha_faced_count);

        labels.push(data_bins_key);

        key_id = key_id + 1
    }

    sorted = double_bubble_sort(res, labels)

    return {
        data_bins: sorted.array_1,
        labels: sorted.array_2
    }
}

const sentenceCase = (str) => {
    if ((str === null) || (str === ''))
        return false;
    else
        str = str.toString();

    return str.replace(/\w\S*/g, function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

function daysBetween(date1, date2) {
    date1 = Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate());
    date2 = Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate());
    var ms = Math.abs(date1 - date2);
    return Math.floor(ms / 1000 / 60 / 60 / 24); //floor should be unnecessary, but just in case
}

function dateFormat(date) {
    // var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    //     'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
    // ];
    //
    // var date = new Date(date)
    // date.setDate(date.getDate())
    // date = monthNames[date.getMonth()] + ',' + date.getDate() + ' ' + date.getFullYear()
    return date.toISOString().split('T')[0]
}

function double_bubble_sort(arr1, arr2){
    // arr1 is used for sorting but values in
    //      are also moved as values in arr1
    //      are moved
    var len = arr1.length;
    for (var i = len-1; i>=0; i--){
        for(var j = 1; j<=i; j++){
            if(arr1[j-1] < arr1[j]){
                var temp1 = arr1[j-1];
                var temp2 = arr2[j-1];

                arr1[j-1] = arr1[j];
                arr2[j-1] = arr2[j];

                arr1[j] = temp1;
                arr2[j] = temp2;
            }
        }
    }
    return {
        array_1: arr1,
        array_2: arr2
    }
}

const isoCountries = {
    'AF' : 'Afghanistan',
    'AX' : 'Aland Islands',
    'AL' : 'Albania',
    'DZ' : 'Algeria',
    'AS' : 'American Samoa',
    'AD' : 'Andorra',
    'AO' : 'Angola',
    'AI' : 'Anguilla',
    'AQ' : 'Antarctica',
    'AG' : 'Antigua And Barbuda',
    'AR' : 'Argentina',
    'AM' : 'Armenia',
    'AW' : 'Aruba',
    'AU' : 'Australia',
    'AT' : 'Austria',
    'AZ' : 'Azerbaijan',
    'BS' : 'Bahamas',
    'BH' : 'Bahrain',
    'BD' : 'Bangladesh',
    'BB' : 'Barbados',
    'BY' : 'Belarus',
    'BE' : 'Belgium',
    'BZ' : 'Belize',
    'BJ' : 'Benin',
    'BM' : 'Bermuda',
    'BT' : 'Bhutan',
    'BO' : 'Bolivia',
    'BA' : 'Bosnia And Herzegovina',
    'BW' : 'Botswana',
    'BV' : 'Bouvet Island',
    'BR' : 'Brazil',
    'IO' : 'British Indian Ocean Territory',
    'BN' : 'Brunei Darussalam',
    'BG' : 'Bulgaria',
    'BF' : 'Burkina Faso',
    'BI' : 'Burundi',
    'KH' : 'Cambodia',
    'CM' : 'Cameroon',
    'CA' : 'Canada',
    'CV' : 'Cape Verde',
    'KY' : 'Cayman Islands',
    'CF' : 'Central African Republic',
    'TD' : 'Chad',
    'CL' : 'Chile',
    'CN' : 'China',
    'CX' : 'Christmas Island',
    'CC' : 'Cocos (Keeling) Islands',
    'CO' : 'Colombia',
    'KM' : 'Comoros',
    'CG' : 'Congo',
    'CD' : 'Congo, Democratic Republic',
    'CK' : 'Cook Islands',
    'CR' : 'Costa Rica',
    'CI' : 'Cote D\'Ivoire',
    'HR' : 'Croatia',
    'CU' : 'Cuba',
    'CY' : 'Cyprus',
    'CZ' : 'Czech Republic',
    'DK' : 'Denmark',
    'DJ' : 'Djibouti',
    'DM' : 'Dominica',
    'DO' : 'Dominican Republic',
    'EC' : 'Ecuador',
    'EG' : 'Egypt',
    'SV' : 'El Salvador',
    'GQ' : 'Equatorial Guinea',
    'ER' : 'Eritrea',
    'EE' : 'Estonia',
    'ET' : 'Ethiopia',
    'FK' : 'Falkland Islands (Malvinas)',
    'FO' : 'Faroe Islands',
    'FJ' : 'Fiji',
    'FI' : 'Finland',
    'FR' : 'France',
    'GF' : 'French Guiana',
    'PF' : 'French Polynesia',
    'TF' : 'French Southern Territories',
    'GA' : 'Gabon',
    'GM' : 'Gambia',
    'GE' : 'Georgia',
    'DE' : 'Germany',
    'GH' : 'Ghana',
    'GI' : 'Gibraltar',
    'GR' : 'Greece',
    'GL' : 'Greenland',
    'GD' : 'Grenada',
    'GP' : 'Guadeloupe',
    'GU' : 'Guam',
    'GT' : 'Guatemala',
    'GG' : 'Guernsey',
    'GN' : 'Guinea',
    'GW' : 'Guinea-Bissau',
    'GY' : 'Guyana',
    'HT' : 'Haiti',
    'HM' : 'Heard Island & Mcdonald Islands',
    'VA' : 'Holy See (Vatican City State)',
    'HN' : 'Honduras',
    'HK' : 'Hong Kong',
    'HU' : 'Hungary',
    'IS' : 'Iceland',
    'IN' : 'India',
    'ID' : 'Indonesia',
    'IR' : 'Iran, Islamic Republic Of',
    'IQ' : 'Iraq',
    'IE' : 'Ireland',
    'IM' : 'Isle Of Man',
    'IL' : 'Israel',
    'IT' : 'Italy',
    'JM' : 'Jamaica',
    'JP' : 'Japan',
    'JE' : 'Jersey',
    'JO' : 'Jordan',
    'KZ' : 'Kazakhstan',
    'KE' : 'Kenya',
    'KI' : 'Kiribati',
    'KR' : 'Korea',
    'KW' : 'Kuwait',
    'KG' : 'Kyrgyzstan',
    'LA' : 'Lao People\'s Democratic Republic',
    'LV' : 'Latvia',
    'LB' : 'Lebanon',
    'LS' : 'Lesotho',
    'LR' : 'Liberia',
    'LY' : 'Libyan Arab Jamahiriya',
    'LI' : 'Liechtenstein',
    'LT' : 'Lithuania',
    'LU' : 'Luxembourg',
    'MO' : 'Macao',
    'MK' : 'Macedonia',
    'MG' : 'Madagascar',
    'MW' : 'Malawi',
    'MY' : 'Malaysia',
    'MV' : 'Maldives',
    'ML' : 'Mali',
    'MT' : 'Malta',
    'MH' : 'Marshall Islands',
    'MQ' : 'Martinique',
    'MR' : 'Mauritania',
    'MU' : 'Mauritius',
    'YT' : 'Mayotte',
    'MX' : 'Mexico',
    'FM' : 'Micronesia, Federated States Of',
    'MD' : 'Moldova',
    'MC' : 'Monaco',
    'MN' : 'Mongolia',
    'ME' : 'Montenegro',
    'MS' : 'Montserrat',
    'MA' : 'Morocco',
    'MZ' : 'Mozambique',
    'MM' : 'Myanmar',
    'NA' : 'Namibia',
    'NR' : 'Nauru',
    'NP' : 'Nepal',
    'NL' : 'Netherlands',
    'AN' : 'Netherlands Antilles',
    'NC' : 'New Caledonia',
    'NZ' : 'New Zealand',
    'NI' : 'Nicaragua',
    'NE' : 'Niger',
    'NG' : 'Nigeria',
    'NU' : 'Niue',
    'NF' : 'Norfolk Island',
    'MP' : 'Northern Mariana Islands',
    'NO' : 'Norway',
    'OM' : 'Oman',
    'PK' : 'Pakistan',
    'PW' : 'Palau',
    'PS' : 'Palestinian Territory, Occupied',
    'PA' : 'Panama',
    'PG' : 'Papua New Guinea',
    'PY' : 'Paraguay',
    'PE' : 'Peru',
    'PH' : 'Philippines',
    'PN' : 'Pitcairn',
    'PL' : 'Poland',
    'PT' : 'Portugal',
    'PR' : 'Puerto Rico',
    'QA' : 'Qatar',
    'RE' : 'Reunion',
    'RO' : 'Romania',
    'RU' : 'Russian Federation',
    'RW' : 'Rwanda',
    'BL' : 'Saint Barthelemy',
    'SH' : 'Saint Helena',
    'KN' : 'Saint Kitts And Nevis',
    'LC' : 'Saint Lucia',
    'MF' : 'Saint Martin',
    'PM' : 'Saint Pierre And Miquelon',
    'VC' : 'Saint Vincent And Grenadines',
    'WS' : 'Samoa',
    'SM' : 'San Marino',
    'ST' : 'Sao Tome And Principe',
    'SA' : 'Saudi Arabia',
    'SN' : 'Senegal',
    'RS' : 'Serbia',
    'SC' : 'Seychelles',
    'SL' : 'Sierra Leone',
    'SG' : 'Singapore',
    'SK' : 'Slovakia',
    'SI' : 'Slovenia',
    'SB' : 'Solomon Islands',
    'SO' : 'Somalia',
    'ZA' : 'South Africa',
    'GS' : 'South Georgia And Sandwich Isl.',
    'ES' : 'Spain',
    'LK' : 'Sri Lanka',
    'SD' : 'Sudan',
    'SR' : 'Suriname',
    'SJ' : 'Svalbard And Jan Mayen',
    'SZ' : 'Swaziland',
    'SE' : 'Sweden',
    'CH' : 'Switzerland',
    'SY' : 'Syrian Arab Republic',
    'TW' : 'Taiwan',
    'TJ' : 'Tajikistan',
    'TZ' : 'Tanzania',
    'TH' : 'Thailand',
    'TL' : 'Timor-Leste',
    'TG' : 'Togo',
    'TK' : 'Tokelau',
    'TO' : 'Tonga',
    'TT' : 'Trinidad And Tobago',
    'TN' : 'Tunisia',
    'TR' : 'Turkey',
    'TM' : 'Turkmenistan',
    'TC' : 'Turks And Caicos Islands',
    'TV' : 'Tuvalu',
    'UG' : 'Uganda',
    'UA' : 'Ukraine',
    'AE' : 'United Arab Emirates',
    'GB' : 'United Kingdom',
    'US' : 'United States',
    'UM' : 'United States Outlying Islands',
    'UY' : 'Uruguay',
    'UZ' : 'Uzbekistan',
    'VU' : 'Vanuatu',
    'VE' : 'Venezuela',
    'VN' : 'Viet Nam',
    'VG' : 'Virgin Islands, British',
    'VI' : 'Virgin Islands, U.S.',
    'WF' : 'Wallis And Futuna',
    'EH' : 'Western Sahara',
    'YE' : 'Yemen',
    'ZM' : 'Zambia',
    'ZW' : 'Zimbabwe'
};

module.exports = {
    pool,
    classify_by_date: classify_by_date,
    classify_for_pie_chart: classify_for_pie_chart,
    isoCountries: isoCountries,
    sentenceCase: sentenceCase,
    append_where: append_where,
    rename_query_params: rename_query_params,
    bad_request: function() {
        return {
            error: {
                status: 400,
                message: 'Bad Request'
            }
        }
    },
    internal_server_error: function() {
        return {
            error: {
                status: 500,
                message: 'Internal Server Error'
            }
        }
    }
}

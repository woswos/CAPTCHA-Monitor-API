var express = require('express');
var router = express.Router();
const utils = require('../utils');

/**
@api {get} /measurements/ Get the list of the completed measurements
@apiVersion 0.0.1
@apiName GetMeasurements
@apiGroup Measurements

@apiParam (Query Parameters) {Number} [limit=100] Used for limiting the number
of results returned in a single call
@apiParam (Query Parameters) {Number} [offset=0] Used for offsetting the cursor
and get data starting from a specific location in the database. A combination of
`limit` and `offset` can be used to get data from the database in pages.
@apiParam (Query Parameters) {Datetime} [before] The start datetime when
measurements were run. Accepts almost all forms of reasonable datetime input
(ex. `22 July 2020`, `July 22, 2020`, `2020-07-22`, `2020-07-22 9:30:20`).
<br> Use ISO-8601 date format if you don't want your input to be interpreted by the
database engine.
@apiParam (Query Parameters) {Datetime} [after]  The end datetime when
measurements were run. Accepts almost all forms of reasonable datetime input
(ex. `22 July 2020`, `July 22, 2020`, `2020-07-22`, `2020-07-22 9:30:20`).
<br> Use ISO-8601 date format if you don't want your input to be interpreted by
the database engine.
@apiParam (Query Parameters) {String} [method]  The name of tool that was used
used to perform the measurement
@apiParam (Query Parameters) {String} [url]  The URL that was used used to
perform the measurement
@apiParam (Query Parameters) {Boolean} [is_captcha_found]  `0` or `1` indicating
whether CAPTCHA was detected
@apiParam (Query Parameters) {Boolean} [is_data_modified]  `0` or `1` indicating
whether the data has been modified
@apiParam (Query Parameters) {String} [exit_node]  The IPv4 address of the exit
relay/node that was used used to perform the measurement
@apiParam (Query Parameters) {String} [tbb_security_level]  The security level
of Tor Browser (only useful when `method` is set to `tor_browser`)
@apiParam (Query Parameters) {String} [captchamonitor_version]  The CAPTCHA
Monitor version that was used used to perform the measurement
@apiParam (Query Parameters) {String} [count_only]  `count_only` is a meta parameter
that is useful for quickly getting the total number of results returned for a
given query. Using `count_only` bypasses the `limit` and `offset` parameters and
performs the query over the whole table. For example, if you want to get the total
number of measurement done with a specific method, you can set `count_only`
to `1` for only getting only this data. It is useful for saving bandwidth and
decreasing query times as well.

@apiSuccess {Object} metadata  The metadata regarding the returned data.
<br> *Note: The specified valid query parameters will also show up under the
metadata key*
@apiSuccess {Number} metadata.count  The number of results returned in this call
@apiSuccess {Number} metadata.limit  The maximum number of results that could be
returned in this call
@apiSuccess {Number} metadata.offset  The data offset in this call
@apiSuccess {List} results The list of objects representing the details of each
measurement, see the `/measurements/details` for the specific format of the objects.

@apiError (400 Bad Request) BadRequest Possibly there is a problem with the
parameters you provided
@apiError (500 Internal Server Error) InternalServerError The server encountered
an internal error
*/
router.get('/', function(req, res, next) {
    let response = {
        metadata: {}
    }
    let params = [];
    let before = req.query.before || ''
    let after = req.query.after || ''
    let limit = Number(req.query.limit) || 100
    let offset = Number(req.query.offset) || 0
    let count_only = Number(req.query.count_only) || 0

    let where_params = {
        method: req.query.method || '',
        url: req.query.url || '',
        is_captcha_found: req.query.is_captcha_found || '',
        is_data_modified: req.query.is_data_modified || '',
        exit_node: req.query.exit_node || '',
        tbb_security_level: req.query.tbb_security_level || '',
        captchamonitor_version: req.query.captchamonitor_version || ''
    }

    let query = ''

    if(count_only != 1){
        query = 'SELECT * FROM results '
    }else{
        query = 'SELECT count(*) FROM results '
    }

    for (let param_name in where_params) {
        if (where_params[param_name] != '') {
            query = utils.append_where(query, param_name + ' = %s ')
            params.push(where_params[param_name])
            response.metadata[param_name] = where_params[param_name]
        }
    }

    if (before != '') {
        if (after != '') {
            query = utils.append_where(query, ' timestamp BETWEEN %s::timestamp AND %s::timestamp ')
            params.push(after, before)
            response.metadata['before'] = before
            response.metadata['after'] = after

        } else {
            query = utils.append_where(query, ' timestamp < %s::timestamp ')
            params.push(before)
            response.metadata['before'] = before
        }

    } else if (after != '') {
        query = utils.append_where(query, ' timestamp > %s::timestamp ')
        params.push(after)
        response.metadata['after'] = after
    }

    if(count_only != 1){
        query += ' ORDER BY id ASC LIMIT %s OFFSET %s '
        params.push(limit, offset)
    }

    // Rename each %s to $[n] in the proper order
    query = utils.rename_query_params(query, params)

    utils.pool.query(query, params, (error, results) => {
        if (error) {
            res.status(400).send(utils.bad_request());
            return
        }

        if(count_only != 1){
            response.results = results.rows
            response.metadata['count'] = response.results.length
            response.metadata['limit'] = limit
            response.metadata['offset'] = offset
        }else{
            response.count = results.rows[0]['count']
        }

        res.status(200).json(response)
    })

});


/**
@api {get} /measurements/count Get the total number of completed measurements
@apiVersion 0.0.1
@apiName CountMeasurements
@apiGroup Measurements

@apiSuccess {Number} count  The total number of completed measurements

@apiError (500 Internal Server Error) InternalServerError The server encountered
an internal error
*/
router.get('/count', function(req, res, next) {
    let query = 'SELECT count("id")::INTEGER FROM results';

    utils.pool.query(query, (error, results) => {
        if (error) {
            res.status(500).send(utils.internal_server_error());
            return
        }

        res.status(200).json(results.rows[0])

    })

});


/**
@api {get} /measurements/details/:id Get the details of a measurements with a given ID
@apiVersion 0.0.1
@apiName MeasurementDetails
@apiGroup Measurements

@apiSuccess {Number} id  The unique ID of the measurement
@apiSuccess {String} timestamp The timestamp of when the measurement was completed
in ISO-8601 date format in UTC
@apiSuccess {String} method Enum: `tor_browser`, `firefox`, `firefox_over_tor`,
`chromium`, `chromium_over_tor`, `brave`, `brave_over_tor`, `curl`,
`curl_over_tor`, `requests`, `requests_over_tor`
<br> The name of the tool that was used to perform the measurement
@apiSuccess {String} url The URL that was used to perform the measurement
@apiSuccess {String} captcha_sign The string that was used to perform the CAPTCHA
detection
@apiSuccess {String} html_data The HTML data that was received during the measurement
@apiSuccess {Boolean} is_captcha_found `0` or `1` indicating whether CAPTCHA was
detected in `html_data`
@apiSuccess {Object} requests A JSON object of the HTTP requests that were made
during the measurement
@apiSuccess {String} exit_node The specific exit node used during the measurement
@apiSuccess {String} captchamonitor_version The specific version of CAPTCHA Monitor
that was used to perform the measurement
@apiSuccess {String} browser_version The specific version of the web browser
(see `method` parameter) that was used to perform the measurement
@apiSuccess {String} [tbb_security_level] The security level of Tor Browser, if
Tor Browser was used for the measurement
@apiSuccess {String} [expected_hash] The expected MD5 hash of the web page at the
given URL. This parameter is used for determining if the received HTML data was
modified by a third-party. For example, useful for checking if the CDN provider
injected any code.
@apiSuccess {Boolean} [is_data_modified] `0` or `1` indicating whether the data
has been modified by a third-party based on the expected_hash parameter

@apiError (204 No Content) NoContent "No Content" will be sent if there is the
call is successful, but there is no measurement matching the given id

@apiError (400 Bad Request) BadRequest Possibly there is a problem with the
parameters you provided
@apiError (500 Internal Server Error) InternalServerError The server encountered
an internal error
*/
router.get('/details/:id', function(req, res, next) {
    let query = 'SELECT * FROM results WHERE id = $1';
    let params = [req.params.id]

    utils.pool.query(query, params, (error, results) => {
        if (error) {
            res.status(400).send(utils.bad_request());
            return
        }

        let response = results.rows[0];
        if (typeof response == 'undefined') {
            res.status(204).send();
        } else {
            res.status(200).json(response);
        }

    })

});

router.get('/details', function(req, res, next) {
    res.status(400).send({
        error: {
            status: 400,
            message: 'Please specify a measurement ID'
        }
    })
});


module.exports = router;

var express = require('express');
var router = express.Router();
const utils = require('../utils');

let sql_get_groupped_urls_by_is_captcha_found = `
    SELECT results.timestamp::date, results.url, results.is_captcha_found, urls.is_https, urls.supports_ipv4, urls.supports_ipv6, count(*) AS "count"
    FROM results
    INNER JOIN urls on results.url = urls.url
    WHERE timestamp > current_date - interval '30 days'
    GROUP BY results.is_captcha_found, results.url, urls.is_https, urls.supports_ipv4, urls.supports_ipv6, results.timestamp::date
    ORDER BY timestamp::date DESC
`;

router.get('/tbb_security_levels', function(req, res, next) {
    let query = `
        SELECT timestamp::date, tbb_security_level, is_captcha_found, count(*) AS "count"
        FROM results
        WHERE results.method = 'tor_browser' AND timestamp > current_date - interval '30 days'
        GROUP BY tbb_security_level, is_captcha_found, timestamp::date
        ORDER BY timestamp::date DESC
        `;

    utils.pool.query(query, (error, results) => {
        if (error) {
            res.status(500).send(utils.internal_server_error());
            return
        }

        let classified = utils.classify_by_date(results.rows, 'tbb_security_level')

        let response = {
            "message": "success",
            "results": {
                "labels": classified.labels,
                "data": {
                    "Low (Standard)": classified.data_bins.low,
                    "Medium (Safer)": classified.data_bins.medium,
                    "High (Safest)": classified.data_bins.high
                }
            }
        }

        res.status(200).json(response)

    })

});

router.get('/tbb_versions', function(req, res, next) {
    let query = `
        SELECT timestamp::date, browser_version, is_captcha_found, count(*) AS "count"
        FROM results
        WHERE results.method = 'tor_browser' AND timestamp > current_date - interval '30 days'
        GROUP BY browser_version, is_captcha_found, timestamp::date
        ORDER BY timestamp::date DESC
        `;

    utils.pool.query(query, (error, results) => {
        if (error) {
            res.status(500).send(utils.internal_server_error());
            return
        }

        let classified = utils.classify_by_date(results.rows, 'browser_version')

        let data = {}
        for (let key in classified.data_bins) {
            data[key] = classified.data_bins[key];
        }

        let response = {
            "message": "success",
            "results": {
                "labels": classified.labels,
                "data": data
            }
        }

        res.status(200).json(response)

    })

});

router.get('/web_browser_type', function(req, res, next) {
    let query = `
        SELECT timestamp::date, method, is_captcha_found, count(*) AS "count"
        FROM results
        WHERE timestamp > current_date - interval '30 days'
        GROUP BY method, is_captcha_found, timestamp::date
        ORDER BY timestamp::date DESC
        `;

    utils.pool.query(query, (error, results) => {
        if (error) {
            res.status(500).send(utils.internal_server_error());
            return
        }

        let classified = utils.classify_by_date(results.rows, 'method')

        let data = {}
        for (let key in classified.data_bins) {
            data[utils.sentenceCase(key.split('_').join(' '))] = classified.data_bins[key];
        }

        let response = {
            "message": "success",
            "results": {
                "labels": classified.labels,
                "data": data
            }
        }

        res.status(200).json(response)

    })

});

router.get('/http_vs_https', function(req, res, next) {
    let query = sql_get_groupped_urls_by_is_captcha_found;

    utils.pool.query(query, (error, results) => {
        if (error) {
            res.status(500).send(utils.internal_server_error());
            return
        }

        https_map = {
            1: 'https',
            0: 'http'
        };

        let rows = results.rows;

        for (let i = 0; i < rows.length; i++) {
            rows[i].http_status = https_map[rows[i].is_https]
        }

        let classified = utils.classify_by_date(rows, 'http_status')

        let response = {
            "message": "success",
            "results": {
                "labels": classified.labels,
                "data": {

                    "HTTP": classified.data_bins.http,
                    "HTTPS": classified.data_bins.https,
                }
            }
        }

        res.status(200).json(response)

    })

});

router.get('/ip_versions', function(req, res, next) {
    let query = sql_get_groupped_urls_by_is_captcha_found;

    utils.pool.query(query, (error, results) => {
        if (error) {
            res.status(500).send(utils.internal_server_error());
            return
        }

        https_map = {
            1: 'https',
            0: 'http'
        };

        let rows = results.rows;

        for (let i = 0; i < rows.length; i++) {
            if (rows[i].supports_ipv6 == '1') {
                rows[i].ip_version = 'ipv6'
            } else {
                rows[i].ip_version = 'ipv4'
            }

        }

        let classified = utils.classify_by_date(rows, 'ip_version')

        let response = {
            "message": "success",
            "results": {
                "labels": classified.labels,
                "data": {
                    "IPv4": classified.data_bins.ipv4,
                    "IPv6": classified.data_bins.ipv6,
                }
            }
        }

        res.status(200).json(response)

    })

});

router.get('/single_vs_multiple_http_reqs', function(req, res, next) {
    let query = sql_get_groupped_urls_by_is_captcha_found;

    utils.pool.query(query, (error, results) => {
        if (error) {
            res.status(500).send(utils.internal_server_error());
            return
        }

        let single_list = ['http://captcha.wtf',

           'https://captcha.wtf',
            'http://exit11.online',
            'https://exit11.online'
        ]

        let rows = results.rows;

        for (let i = 0; i < rows.length; i++) {
            if (single_list.indexOf(rows[i].url) >= 0) {
                rows[i].http_req_status = 'single'
            } else {
                rows[i].http_req_status = 'multiple'
            }

        }

        let classified = utils.classify_by_date(rows, 'http_req_status')

        let response = {
            "message": "success",
            "results": {
                "labels": classified.labels,
                "data": {
                    "Single request": classified.data_bins.single,
                    "Multiple requests": classified.data_bins.multiple,
                }
            }
        }

        res.status(200).json(response)

    })

});

router.get('/country', function(req, res, next) {
    let query = `
        SELECT results.is_captcha_found, relays.country, count(*) AS "count"
        FROM results
        INNER JOIN relays on results.exit_node = relays.address
        WHERE results.is_captcha_found = '1'
        GROUP BY results.is_captcha_found, relays.country
        `;

    utils.pool.query(query, (error, results) => {
        if (error) {
            res.status(500).send(utils.internal_server_error());
            return
        }

        let classified = utils.classify_for_pie_chart(results.rows, 'country')

        let data = {};
        let total = 0;
        for (let key in classified.data_bins) {
            if (classified.data_bins[key] != 0) {
                data[key] = classified.data_bins[key];
                total += classified.data_bins[key]
            }
        }

        // for (let key in data) {
        //     data[key] = ((data[key] / total) * 100).toFixed(2);
        // }

        let converted_labels = [];
        for (let i = 0; i < classified.labels.length; i++) {
            if (classified.labels[i] != 'null') {
                converted_labels.push(utils.isoCountries[classified.labels[i]])
            }
        }

        let response = {
            "message": "success",
            "results": {
                "labels": converted_labels,
                "data": data
            }
        }

        res.status(200).json(response)

    })

});

router.get('/continent', function(req, res, next) {
    let query = `
    SELECT results.is_captcha_found, relays.continent, count(*) AS "count"
    FROM results
    INNER JOIN relays on results.exit_node = relays.address
    WHERE results.is_captcha_found = '1'
    GROUP BY results.is_captcha_found, relays.continent
        `;

    utils.pool.query(query, (error, results) => {
        if (error) {
            res.status(500).send(utils.internal_server_error());
            return
        }

        let classified = utils.classify_for_pie_chart(results.rows, 'continent')

        let data = {};
        let total = 0;
        for (let key in classified.data_bins) {
            if (classified.data_bins[key] != 0) {
                data[key] = classified.data_bins[key];
                total += classified.data_bins[key]
            }
        }

        // for (let key in data) {
        //     data[key] = ((data[key] / total) * 100).toFixed(2);
        // }

        let labels_updated = [];
        for (let key in classified.labels) {
            if (classified.labels[key] != 'null') {
                labels_updated.push(classified.labels[key])
            }
        }

        let response = {
            "message": "success",
            "results": {
                "labels": labels_updated,
                "data": data
            }
        }
        res.status(200).json(response)

    })

});

router.get('/', function(req, res, next) {
    res.status(400).send(utils.bad_request())
});

module.exports = router;

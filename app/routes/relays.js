var express = require('express');
var router = express.Router();
const utils = require('../utils');

/**
@api {get} /relays/ Get the list of the relays that have been used in the measurements
@apiVersion 0.0.1
@apiName GetRelays
@apiGroup Relays

@apiParam (Query Parameters) {Number} [limit=100] Used for limiting the number of
 results returned in a single call
@apiParam (Query Parameters) {Number} [offset=0] Used for offsetting the cursor
and get data starting from a specific location in the database. A combination of
`limit` and `offset` can be used to get data from the database in pages.
@apiParam (Query Parameters) {String} [nickname] Filter by relay nickname
@apiParam (Query Parameters) {String} [address]  Filter by relay IPv4 address
@apiParam (Query Parameters) {Boolean} [is_ipv4_exiting_allowed]  `0` or `1` to
filter whether IPv4 exitting is allowed
@apiParam (Query Parameters) {Boolean} [is_ipv6_exiting_allowed]  `0` or `1` to
filter whether IPv6 exitting is allowed
@apiParam (Query Parameters) {String} [country]  Filter by country in
ISO 3166-1 alpha-2 format
@apiParam (Query Parameters) {String} [continent]  Filter by continent
<br> Enum: `Africa`, `Antarctica`, `Asia`, `Europe`, `North America`, `Oceania`,
`South America`
@apiParam (Query Parameters) {String} [status]  Filter by relay status
<br> Enum: `online`, `offline`
@apiParam (Query Parameters) {String} [count_only]  `count_only` is a meta parameter
that is useful for quickly getting the total number of results returned for a
given query. Using `count_only` bypasses the `limit` and `offset` parameters and
performs the query over the whole table. For example, if you want to get the total
number of relays in a specific country, you can set `count_only` to `1` for only
getting only this data. It is useful for saving bandwidth and decreasing query times
as well.

@apiSuccess {Object} metadata  The metadata regarding the returned data.
<br> *Note: The specified valid query parameters will also show up under the
metadata key*
@apiSuccess {Number} metadata.count The number of results returned in this call
@apiSuccess {Number} metadata.limit The maximum number of results that could be
returned in this call
@apiSuccess {Number} metadata.offset The data offset in this call
@apiSuccess {List} results The list of objects representing the details of each
relay, see the `/relays/details` for the specific format of the objects.

@apiError (400 Bad Request) BadRequest Possibly there is a problem with the
parameters you provided
@apiError (500 Internal Server Error) InternalServerError The server encountered
an internal error
*/
router.get('/', function(req, res, next) {
    let response = {
        metadata: {}
    }
    let params = [];
    let limit = Number(req.query.limit) || 100
    let offset = Number(req.query.offset) || 0
    let count_only = Number(req.query.count_only) || 0

    let where_params = {
        address: req.query.address || '',
        is_ipv4_exiting_allowed: req.query.is_ipv4_exiting_allowed || '',
        is_ipv6_exiting_allowed: req.query.is_ipv6_exiting_allowed || '',
        country: req.query.country || '',
        continent: req.query.continent || '',
        status: req.query.status || '',
        nickname: req.query.nickname || ''
    }

    let query = ''

    if(count_only != 1){
        query = 'SELECT * FROM relays '
    }else{
        query = 'SELECT count(*) FROM relays '
    }

    for (let param_name in where_params) {
        if (where_params[param_name] != '') {
            query = utils.append_where(query, param_name + ' = %s ')
            params.push(where_params[param_name])
            response.metadata[param_name] = where_params[param_name]
        }
    }

    if(count_only != 1){
        query += ' ORDER BY id ASC LIMIT %s OFFSET %s '
        params.push(limit, offset)
    }

    // Rename each %s to $[n] in the proper order
    query = utils.rename_query_params(query, params)

    utils.pool.query(query, params, (error, results) => {
        if (error) {
            res.status(400).send(utils.bad_request());
            return
        }

        if(count_only != 1){
            response.results = results.rows
            response.metadata['count'] = response.results.length
            response.metadata['limit'] = limit
            response.metadata['offset'] = offset
        }else{
            response.count = results.rows[0]['count']
        }

        res.status(200).json(response)
    })

});


/**
@api {get} /relays/count Get the number of relays in the database
@apiVersion 0.0.1
@apiName CountRelays
@apiGroup Relays

@apiSuccess {Number} count  The total number of relays used for testing

@apiError (500 Internal Server Error) InternalServerError The server encountered
an internal error
*/
router.get('/count', function(req, res, next) {
    let query = 'SELECT count("id")::INTEGER FROM relays';

    utils.pool.query(query, (error, results) => {
        if (error) {
            res.status(500).send(utils.internal_server_error());
            return
        }

        res.status(200).json(results.rows)

    })

});


/**
@api {get} /relays/details/:fingerprint Get the details of a relay with a given fingerprint
@apiVersion 0.0.1
@apiName RelayDetails
@apiGroup Relays

@apiDescription This endpoint only provides information related to the CAPTCHA
Monitor. Check [https://metrics.torproject.org/](https://metrics.torproject.org/)
    for more information about the specific relays.

@apiSuccess {Number} id  The unique ID of the relay
@apiSuccess {String} last_updated The timestamp of when the relay details were
updated in ISO-8601 date format
@apiSuccess {String} fingerprint The fingerprint of the relay
@apiSuccess {String} address IPv4 address of the relay
@apiSuccess {Boolean} is_ipv4_exiting_allowed 0 or 1 indicating whether the relay
allows IPv4 exitting
@apiSuccess {Boolean} is_ipv6_exiting_allowed 0 or 1 indicating whether the relay
allows IPv6 exitting
@apiSuccess {String} country The predicted physical location (in terms of the
    country) of the relay in ISO 3166-1 alpha-2 format
@apiSuccess {String} continent The predicted physical location (in terms of the
continent) of the relay. Enum: `Africa`, `Antarctica`,
`Asia`, `Europe`, `North America`, `Oceania`, `South America`
@apiSuccess {String} status  Enum: `online` or `offline` indicating whether the
relay was in the latest consensus
@apiSuccess {String} nickname Nickname of the relay
@apiSuccess {Float} captcha_probability The calculated CAPTCHA probability of the
relay based on its measurement history
@apiSuccess {Object} performed_tests A JSON object of the tests performed using
this relay. If a test is repeated from this list, only the date of the latest
measurement will be available here.

@apiError (204 No Content) NoContent "No Content" will be sent if there is no
relay matching the given fingerprint

@apiError (400 Bad Request) BadRequest Possibly there is a problem with the
parameters you provided
@apiError (500 Internal Server Error) InternalServerError The server encountered
an internal error
*/
router.get('/details/:fingerprint', function(req, res, next) {
    let query = 'SELECT * FROM relays WHERE fingerprint = $1';
    let params = [req.params.fingerprint]

    utils.pool.query(query, params, (error, results) => {
        if (error) {
            res.status(400).send(utils.bad_request());
            return
        }

        let response = results.rows[0];
        if (typeof response == 'undefined') {
            res.status(204).send();
        } else {
            res.status(200).json(response);
        }

    })

});


router.get('/details', function(req, res, next) {
    res.status(400).send({
        error: {
            status: 400,
            message: 'Please specify a relay fingerprint'
        }
    })
});


module.exports = router;

var express = require('express');
var router = express.Router();
const utils = require('../utils');

/**
@api {get} /tests/urls/ Get the URLs used for testing
@apiVersion 0.0.1
@apiName GetUrls
@apiGroup Tests

@apiParam (Query Parameters) {Number} [limit=100] Used for limiting the number
of results returned in a single call
@apiParam (Query Parameters) {Number} [offset=0] Used for offsetting the cursor
and get data starting from a specific location in the database. A combination
of `limit` and `offset` can be used to get data from the database in pages.
@apiParam (Query Parameters) {String} [url]  Filter by URL
@apiParam (Query Parameters) {Boolean} [is_https]  `0` or `1` to filter whether
URL is using SSL
@apiParam (Query Parameters) {Boolean} [supports_ipv4]  `0` or `1` to filter
whether the domain accepts IPv4 connections
@apiParam (Query Parameters) {Boolean} [supports_ipv6]  `0` or `1` to filter
whether the domain accepts IPv6 connections
@apiParam (Query Parameters) {String} [captcha_sign]  Filter by CAPTCHA sign
@apiParam (Query Parameters) {String} [cdn_provider]  Filter by CDN provider

@apiSuccess {Object} metadata  The metadata regarding the returned data.
<br> *Note: The specified query parameters will also show up under the metadata key*
@apiSuccess {Number} metadata.count The number of results returned in this call
@apiSuccess {Number} metadata.limit The maximum number of results that could be
returned in this call
@apiSuccess {Number} metadata.offset The data offset in this call
@apiSuccess {List} results The list of objects representing the details of each URL

@apiError (400 Bad Request) BadRequest Possibly there is a problem with the
parameters you provided
@apiError (500 Internal Server Error) InternalServerError The server encountered
an internal error
*/
router.get('/urls', function(req, res, next) {
    let response = {
        metadata: {}
    }
    let params = [];
    let limit = Number(req.query.limit) || 100
    let offset = Number(req.query.offset) || 0

    let where_params = {
        url: req.query.url || '',
        supports_ipv4: req.query.supports_ipv4 || '',
        supports_ipv6: req.query.supports_ipv6 || '',
        captcha_sign: req.query.captcha_sign || '',
        cdn_provider: req.query.cdn_provider || ''
    }

    let query = 'SELECT * FROM urls '

    for (let param_name in where_params) {
        if (where_params[param_name] != '') {
            query = utils.append_where(query, param_name + ' = %s ')
            params.push(where_params[param_name])
            response.metadata[param_name] = where_params[param_name]
        }
    }

    query += ' ORDER BY id ASC LIMIT %s OFFSET %s '
    params.push(limit, offset)

    // Rename each %s to $[n] in the proper order
    query = utils.rename_query_params(query, params)

    utils.pool.query(query, params, (error, results) => {
        if (error) {
            res.status(400).send(utils.bad_request());
            return
        }

        response.results = results.rows;
        response.metadata['count'] = response.results.length
        response.metadata['limit'] = limit
        response.metadata['offset'] = offset

        res.status(200).json(response)
    })

});


/**
@api {get} /tests/urls/count Get the total number of URLs used for testing
@apiVersion 0.0.1
@apiName CountUrls
@apiGroup Tests

@apiSuccess {Number} count  The total number of URLs used for testing

@apiError (500 Internal Server Error) InternalServerError The server encountered
an internal error
*/
router.get('/urls/count', function(req, res, next) {
    let query = 'SELECT count("id")::INTEGER FROM urls';

    utils.pool.query(query, (error, results) => {
        if (error) {
            res.status(500).send(utils.internal_server_error());
            return
        }

        res.status(200).json(results.rows[0])

    })

});


/**
@api {get} /tests/fetchers/ Get the fetchers used for testing
@apiVersion 0.0.1
@apiName GetFetchers
@apiGroup Tests

@apiParam (Query Parameters) {Number} [limit=100] Used for limiting the number
of results returned in a single call
@apiParam (Query Parameters) {Number} [offset=0] Used for offsetting the cursor
and get data starting from a specific location in the database. A combination of
 `limit` and `offset` can be used to get data from the database in pages.

@apiSuccess {Object} metadata  The metadata regarding the returned data.
<br> *Note: The specified valid query parameters will also show up under the
metadata key*
@apiSuccess {Number} metadata.count The number of results returned in this call
@apiSuccess {Number} metadata.limit The maximum number of results that could be
returned in this call
@apiSuccess {Number} metadata.offset The data offset in this call
@apiSuccess {List} results The list of objects representing the details of each
fetcher

@apiError (400 Bad Request) BadRequest Possibly there is a problem with the
parameters you provided
@apiError (500 Internal Server Error) InternalServerError The server encountered
an internal error
*/
router.get('/fetchers', function(req, res, next) {
    let response = {
        metadata: {}
    }
    let params = [];
    let limit = Number(req.query.limit) || 100
    let offset = Number(req.query.offset) || 0

    let where_params = {
    }

    let query = 'SELECT * FROM fetchers '

    for (let param_name in where_params) {
        if (where_params[param_name] != '') {
            query = utils.append_where(query, param_name + ' = %s ')
            params.push(where_params[param_name])
            response.metadata[param_name] = where_params[param_name]
        }
    }

    query += ' ORDER BY id ASC LIMIT %s OFFSET %s '
    params.push(limit, offset)

    // Rename each %s to $[n] in the proper order
    query = utils.rename_query_params(query, params)

    utils.pool.query(query, params, (error, results) => {
        if (error) {
            res.status(400).send(utils.bad_request());
            return
        }

        response.results = results.rows;
        response.metadata['count'] = response.results.length
        response.metadata['limit'] = limit
        response.metadata['offset'] = offset

        res.status(200).json(response)
    })

});


/**
@api {get} /tests/fetchers/count Get the total number of fetchers used for testing
@apiVersion 0.0.1
@apiName CountFetchers
@apiGroup Tests

@apiSuccess {Number} count  The total number of fetchers used for testing

@apiError (500 Internal Server Error) InternalServerError The server encountered
an internal error
*/
router.get('/fetchers/count', function(req, res, next) {
    let query = 'SELECT count("id")::INTEGER FROM fetchers';

    utils.pool.query(query, (error, results) => {
        if (error) {
            res.status(500).send(utils.internal_server_error());
            return
        }

        res.status(200).json(results.rows[0])

    })

});


router.get('/', function(req, res, next) {
    res.status(400).send({
        error: {
            status: 400,
            message: 'Please specify a test element: /urls or /fetchers'
        }
    })
});


module.exports = router;

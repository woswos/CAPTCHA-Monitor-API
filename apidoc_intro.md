The CAPTCHA Monitoring project aims to track how often Cloudflare fronted websites return CAPTCHAs to Tor clients. The project aims to achieve this by fetching web pages via Tor and other mainstream web browsers and comparing the results. The tests are repeated periodically to find the patterns over time.


Collected metadata, metrics, and results are analyzed and displayed at the dashboard at [dashboard.captcha.wtf](https://dashboard.captcha.wtf/) to understand how Cloudflare manipulates internet traffic and affects people's access to the internet. Please visit the [project website](https://gitlab.torproject.org/woswos/CAPTCHA-Monitor/-/wikis/home) for more detailed information.


The API is mainly used to feed the graphs/tables on the dashboard, but it also servers as a direct data source for individuals who are interested in conduct further research using the dataset.


If you are interested in downloading the whole dataset, I'm happy to send you an SQL dump or something else you prefer. Just [contact](https://gitlab.torproject.org/woswos/CAPTCHA-Monitor/-/wikis/home#contact) me. _I plan to automate this process in the future._

<br>

Feel free create an issue for suggestions, support, contact, and error reporting on [GitLab](https://gitlab.torproject.org/woswos/CAPTCHA-Monitor-API)
